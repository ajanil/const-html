$( document ).ready(function() {
    $('#maintOptions').change(function(){
        var option = $(this).val();
        if(option == 1){
            $('.insurance').toggleClass('d-none');
            $('.workshop, .tax, .fine').addClass('d-none');
        }
        else if(option == 2){
            $('.tax').toggleClass('d-none');
            $('.insurance, .workshop, .fine').addClass('d-none');
        }
        else if(option == 3){
            $('.workshop').toggleClass('d-none');
            $('.insurance, .tax, .fine').addClass('d-none');
        }
        else if(option == 4){
            $('.fine').toggleClass('d-none');
            $('.insurance, .tax, .workshop').addClass('d-none');
        }
    });

    //sales payment mode change
    $('#readyCash').on('click',function(){
        $('#credit, #cheque').parent().find('.show-more').addClass('d-none');
    });
    $('#credit').on('click',function(){
        $(this).parent().find('.show-more').removeClass('d-none');
        $('#cheque').parent().find('.show-more').addClass('d-none');
    });
    $('#cheque').on('click',function(){
        $(this).parent().find('.show-more').removeClass('d-none');
        $('#credit').parent().find('.show-more').addClass('d-none');
    });


    //arrow up-down toggle
    $('a[data-toggle="collapse"]').on('click',function(){
        $(this).find('.fa-chevron-down').toggleClass('rotate-arrow');
    });


    //employee payroll worker type changes
    $('#selectWorkertype').on('change',function(){
       var worker = $(this).val();
       switch(worker){
            case "1": $('#workerName').text('Manager Name');
                    break;
            case "2": $('#workerName').text('Labour Name');
                    break;
            case "3": $('#workerName').text('Staff Name');
                    break;
            case "4": $('#workerName').text('Driver Name');
                    break;
       }
    });

    //expense type changes
    $('#selectExpensetype').on('change',function(){
        var expense = $(this).val();
        switch(expense){
            case "1":   $('.material-expense').removeClass('d-none');
                        $('.wages,.machine-rent,.miscellenous').addClass('d-none');
                        break;
            case "2":   $('.wages').removeClass('d-none');
                        $('.material-expense,.machine-rent,.miscellenous').addClass('d-none');
                        break;
            case "3":   $('.machine-rent').removeClass('d-none');
                        $('.material-expense,.wages,.miscellenous').addClass('d-none');
                        break;
            case "4":   $('.material-expense,.wages,.machine-rent,.miscellenous').addClass('d-none');
                        break;
            case "5":   $('.miscellenous').removeClass('d-none');
                        $('.material-expense,.wages,.machine-rent').addClass('d-none');
                        break;
            default:    $('.material-expense,.wages,.machine-rent,.miscellenous').addClass('d-none');
        }
     });


    //machine rent type changes in expenses page
    $('.hour-type').on('click',function(){       
        $('.hour-wise').removeClass('d-none');
        $('.day-wise,.month-wise,.year-wise').addClass('d-none');
    });
    $('.day-type').on('click',function(){       
        $('.day-wise').removeClass('d-none');
        $('.hour-wise,.month-wise,.year-wise').addClass('d-none');
    });
    $('.month-type').on('click',function(){       
        $('.month-wise').removeClass('d-none');
        $('.day-wise,.hour-wise,.year-wise').addClass('d-none');
    });
    $('.year-type').on('click',function(){       
        $('.year-wise').removeClass('d-none');
        $('.day-wise,.month-wise,.hour-wise').addClass('d-none');
    });



    //employee renumeration changes
    $('#dailyWages').on('click',function(){
        $('.dailyWages').addClass('d-none');
        $('.monthSalary, .salaryData').removeClass('d-none');
    });
    $('#monthSalary').on('click',function(){
        $('.monthSalary').addClass('d-none');
        $('.dailyWages, .salaryData').removeClass('d-none');
    });


    // wage paid button text change
    $(".btn-pay").on('click',function(){
        $(this).text("Paid").addClass('green accent-3');
    });

    // material take button change
    $(".btn-material").on('click',function(){
        $(this).text("Taken").addClass('green accent-3');
    });

    //creditors button change
    $(".btn-credit").on('click',function(){
        $(this).text("Paid").addClass('green accent-3');
    });



    //convert data to pdf file
    $('a[data-pdf="pdf"]').on('click',function(){
        var dataSection = $(this).parents('.table-report').find('table');
        html2canvas(dataSection.get(0)).then(function(canvas) {
            var imgData = canvas.toDataURL('image/jpeg');              
            var doc = new jsPDF();
            doc.setFontSize(25);
            doc.text(90,20,"Report");
            doc.addImage(imgData, 'JPEG', 12,30);
            doc.save('ReportFile.pdf');
        });
    });
});


